import pytest
from click.testing import CliRunner
from gpg_git_store import Store

from ngondro import Practice
from ngondro.__main__ import cli


@pytest.fixture(autouse=True)
def setup(created_store_path):
    store = Store(root=created_store_path, item_class=Practice)

    practice1 = store.create_practice(id=1, name="foo", required_accumulations=10)
    practice1.sessions.create_item(id=1, name="hello", quantity=1, time=1)

    practice2 = store.create_practice(id=3, name="bar", required_accumulations=100)
    practice2.sessions.create_item(id=1, name="hello", quantity=10, time=1)

    practice3 = store.create_practice(id=5, name="boo", required_accumulations=9)
    practice3.sessions.create_item(id=1, name="hello", quantity=10, time=2)


@pytest.fixture
def runner():
    return CliRunner()


def test_cli_create_adds_practice_to_store(runner):
    result = runner.invoke(cli, ["create", "hello"], input="108\n")
    assert result.exit_code == 0

    assert result.output == "Required accumulations: 108\n  [6] hello (0 / 108)\n"


def test_cli_create_sets_accumulations_when_specified(runner):
    result = runner.invoke(cli, ["create", "hello", "-r", "108", "-a", "1"])
    assert result.exit_code == 0

    assert result.output == "  [6] hello (1 / 108)\n"


def test_cli_ls_returns_list_of_practices(runner):
    result = runner.invoke(cli, ["ls"])
    assert result.exit_code == 0

    assert (
        result.output
        == "  [1] foo (1 / 10)\n  [3] bar (10 / 100)\nX [5] boo (10 / 9)\n"
    )


def test_cli_mv_renames_specified_practice(runner):
    result = runner.invoke(cli, ["mv", "5", "noo"])
    assert result.exit_code == 0

    assert result.output == "X [5] noo (10 / 9)\n"


def test_cli_rm_removes_task_when_confirmed(runner):
    result = runner.invoke(cli, ["rm", "3"], input="y\n")
    assert result.exit_code == 0

    assert result.output == "Delete the task? [y/N]: y\nDeleting: bar.\n"


def test_cli_rm_aborts_when_not_confirmed(runner):
    result = runner.invoke(cli, ["rm", "3"], input="n\n")
    assert result.exit_code == 1

    assert result.output == "Delete the task? [y/N]: n\nAborted!\n"


def test_cli_rm_skips_confirmation_when_flagged(runner):
    result = runner.invoke(cli, ["rm", "3", "--force"])
    assert result.exit_code == 0


def test_cli_practice_records_accumulation_of_specified_practice(runner):
    result = runner.invoke(cli, ["practice", "1"], input="108\n")
    assert result.exit_code == 0

    assert result.output == "Quantity: 108\nX [1] foo (109 / 10)\n"
