import shutil

import pytest

from . import TEST_DATA


@pytest.fixture(autouse=True)
def cleanup_test_data():
    def cleanup():
        if TEST_DATA.exists():
            shutil.rmtree(TEST_DATA)

    cleanup()
    TEST_DATA.mkdir()
    yield
    cleanup()


@pytest.fixture(autouse=True)
def mocked_subprocess_run(mocker):
    return mocker.patch("gpg_git_store.subprocess.run")


@pytest.fixture
def store_path():
    return TEST_DATA / ".ngondro-store"


@pytest.fixture
def created_store_path(store_path):
    store_path.mkdir()
    return store_path


@pytest.fixture
def practice_file_path(created_store_path):
    return created_store_path / "example-practice"
