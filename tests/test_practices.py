from gpg_git_store import StoreFile

from ngondro import Practice


def test_practices_accumulations_sums_session_quantities(
    created_store_path, practice_file_path
):
    practice_file = StoreFile(path=practice_file_path)
    practice = Practice(
        id=1, name="example", required_accumulations=100, storage=practice_file
    )

    practice.sessions.create_item(id=1, name="hello", quantity=15, time=1)
    practice.sessions.create_item(id=2, name="hollo", quantity=5, time=2)

    assert practice.accumulations == 20
