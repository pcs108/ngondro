from gpg_git_store import StoreFile

from ngondro import Practice, presenters


def test_practice_presenter_incomplete_practice(practice_file_path):
    practice = Practice(
        storage=StoreFile(practice_file_path),
        id=108,
        name="fooboo",
        required_accumulations=100,
    )
    presenter = presenters.PracticePresenter(practice)
    assert presenter.text == "  [108] fooboo (0 / 100)"


def test_practice_presenter_complete_practice(practice_file_path):
    practice = Practice(
        storage=StoreFile(practice_file_path),
        id=18,
        name="barfoo",
        required_accumulations=108,
    )
    practice.sessions.create_item(name="finished", quantity=109, time=1)
    presenter = presenters.PracticePresenter(practice)
    assert presenter.text == "X [18] barfoo (109 / 108)"
