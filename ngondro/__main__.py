import os
import time
from pathlib import Path

import click
from gpg_git_store import Store, presenters
from gpg_git_store.click import State
from gpg_git_store.click import commands as shared_commands
from gpg_git_store.click import helpers as click_helpers

from . import Practice
from .presenters import PracticePresenter

NOW = time.time()

STORE_DOES_NOT_EXIST_ERROR_MESSAGE = """ngondro has not been set up.
run `ngondro init` before continuing.
"""

HOME = Path(os.path.expanduser("~"))
DEFAULT_STORE_PATH = HOME / ".ngondro-store"

STORE_PATH = Path(os.getenv("NGONDRO_STORE", DEFAULT_STORE_PATH)).absolute()


@click.group()
@click.pass_context
def cli(ctx: click.Context) -> None:
    try:
        store = Store.from_directory(STORE_PATH, item_class=Practice)
    except FileNotFoundError:
        store = None

    ctx.obj = State(
        store_item_class=Practice,
        store=store,
        store_path=STORE_PATH,
        git_commit_messages_for_commands={
            "create": "Added provided practice.",
            "decrypt": "Decrypted the store.",
            "encrypt": "Encrypted the store with specified GPG key.",
            "mv": "Renamed specified practice.",
            "practice": "Added specified accumulations to specified practice.",
            "rm": "Removed specified practice.",
        },
    )


cli.command(shared_commands.init)
cli.command(shared_commands.decrypt)
cli.command(shared_commands.encrypt)
cli.command(shared_commands.git)


@cli.command()
@click.pass_context
@click_helpers.require_store
@click_helpers.save_git_changes
@click.argument("name")
@click.option("-r", "--required_accumulations", type=int, prompt=True, required=True)
@click.option("-a", "--accumulations", type=int, default=0)
def create(
    ctx: click.Context, name: str, required_accumulations: int, accumulations: int
) -> None:
    practice = ctx.obj.store.create_practice(
        name=name,
        required_accumulations=required_accumulations,
    )
    if accumulations:
        practice.sessions.create_item(name="init", quantity=accumulations, time=NOW)
    presenter = PracticePresenter(practice)
    click.echo(presenter.text)


@cli.command()
@click.pass_context
@click_helpers.require_store
def ls(ctx: click.Context) -> None:
    output = presenters.ItemListPresenter(
        items=ctx.obj.store.practice_list,
        item_presenter_class=PracticePresenter,
        empty_list_message="No practices found.",
    ).text
    click.echo(output)


@cli.command()
@click.pass_context
@click_helpers.require_store
@click_helpers.save_git_changes
@click.argument("id", type=int)
@click.argument("name")
def mv(ctx: click.Context, id: int, name: str) -> None:
    practice = ctx.obj.store.practices[id]
    practice.update(name=name)
    practice.save()
    presenter = PracticePresenter(practice)
    click.echo(presenter.text)


@cli.command()
@click.pass_context
@click_helpers.require_store
@click_helpers.save_git_changes
@click.argument("id", type=int)
@click.option("-n", "--name", type=str, default="")
@click.option("-q", "--quantity", type=int, prompt=True, required=True)
def practice(ctx: click.Context, id: int, name: str, quantity: int) -> None:
    pract = ctx.obj.store.practices[id]

    name = name or f"{pract.name}: {NOW}"
    pract.sessions.create_item(name=name, quantity=quantity, time=NOW)
    presenter = PracticePresenter(pract)
    click.echo(presenter.text)


@cli.command()
@click.pass_context
@click_helpers.require_store
@click_helpers.save_git_changes
@click.argument("id", type=int)
@click.option(
    "-f",
    "--force",
    is_flag=True,
    callback=click_helpers.abort_if_false,
    expose_value=False,
    prompt="Delete the task?",
)
def rm(ctx: click.Context, id: int) -> None:
    practice = ctx.obj.store.practices[id]
    click.echo(f"Deleting: {practice.name}.")
    practice.delete()


def main():
    cli()


if __name__ == "__main__":
    main()
