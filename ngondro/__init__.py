from dataclasses import dataclass

from gpg_git_store import RelatedItems, StoreItem

__author__ = "Patrick Schneeweis"
__docformat__ = "markdown en"
__license__ = "GPLv3+"
__title__ = "ngondro"
__version__ = "0.0.1"


@dataclass(slots=True)
class Session(StoreItem):
    quantity: int
    time: float


@dataclass(slots=True)
class Practice(StoreItem):
    required_accumulations: int
    sessions: RelatedItems = RelatedItems(Session)

    @property
    def accumulations(self) -> int:
        return sum([s.quantity for s in self.sessions.item_list])

    @property
    def completed(self) -> bool:
        return self.accumulations >= self.required_accumulations
