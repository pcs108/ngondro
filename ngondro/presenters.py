from gpg_git_store.presenters import ItemPresenter

from . import Practice


class PracticePresenter(ItemPresenter):
    item: Practice

    @property
    def custom_text(self) -> str:
        return f"({self.item.accumulations} / {self.item.required_accumulations})"
